# Might want to fix this at a particular build
FROM peigenesis/php-debian:latest

MAINTAINER "ed.henderson@peigenesis.com"

# Set correct environment variables.
ENV DEBIAN_FRONTEND=noninteractive
ENV HOME /root
ENV PYTHON_BASE_VERSION 3.8
ENV PYTHON_FULL_VERSION 3.8.2

RUN mkdir -p /build \
    && cd /build \
    && pwd \
    && curl -O https://www.python.org/ftp/python/${PYTHON_FULL_VERSION}/Python-${PYTHON_FULL_VERSION}.tar.xz \
    && tar -xf Python-${PYTHON_FULL_VERSION}.tar.xz && pwd && ls -al \
    && cd Python-${PYTHON_FULL_VERSION} \
    && ./configure --enable-optimizations \
    && make -j 4 \
    && make altinstall \
    && python${PYTHON_BASE_VERSION} --version

# Show versions
RUN php -v \
  && python -v

CMD ["bash"]
